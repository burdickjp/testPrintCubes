
// width of the cube
widthCube = 20;

// diameter of the cylinders removed from the cube
diameterCylinder = 16;

// angle to rotate the cylinders
angleRotate = 90;

/*
    comment out a vector to prevent that cylinder from being removed from the cube
*/
vectorRotate= [
    [0,1,0], // cylinder along x direction
    [1,0,0], // cylinder along y direction
    [0,0,1]  // cylinder along z direction
];

difference(){
    cube([widthCube,widthCube,widthCube], center=true);

    for( i= vectorRotate ) {
        rotate( a= angleRotate, v=i ){
            cylinder(d=diameterCylinder, h=widthCube, center=true);
        }
    }
}