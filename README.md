# Test Print Cubes
20 mm test print cubes for additive manufacturing.

There are four cubes: a solid cube, a cube with a cylinder removed, a cube 
with two cylinders removed, and a cube with three cylinders removed.

Each cube can be useful for refining parameters for various additive 
manufacturing systems.

# License

This documentation describes Open Hardware and is licensed under the CERN 
OHL v. 1.2.

You may redistribute and modify this documentation under the terms of the 
CERN OHL v.1.2. (http://ohwr.org/cernohl). This documentation is 
distributed WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF 
MERCHANTABILITY, SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR 
PURPOSE. Please see the CERN OHL v.1.1 for applicable conditions.
